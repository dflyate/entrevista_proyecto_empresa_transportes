/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.transporte.servicio;

import java.util.List;
import javax.ejb.Local;
import com.transporte.domain.Empresa;

/**
 *
 * @author Usuario
 */
@Local
public interface EmpresaService {
    public List<Empresa> listarEmpresas();
    public Empresa encontrarEmpresaPorId(Empresa empresa);
    public void registrarEmpresa(Empresa empresa);
    public void modificarEmpresa(Empresa empresa);
    public void eliminarEmpresa(Empresa empresa);
}
