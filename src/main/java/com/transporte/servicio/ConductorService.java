/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.transporte.servicio;

import java.util.List;
import javax.ejb.Local;
import com.transporte.domain.Conductor;

/**
 *
 * @author Usuario
 */
@Local
public interface ConductorService {
    public List<Conductor> listarConductors();
    public Conductor encontrarConductorPorId(Conductor conductor);
    public void registrarConductor(Conductor conductor);
    public void modificarConductor(Conductor conductor);
    public void eliminarConductor(Conductor conductor);
}
