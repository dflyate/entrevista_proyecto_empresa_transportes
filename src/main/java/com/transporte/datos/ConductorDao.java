/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.transporte.datos;

import java.util.List;
import com.transporte.domain.Conductor;

/**
 *
 * @author Usuario
 */
public interface ConductorDao {
    public List<Conductor> findAllConductors();
    
    public Conductor findConductorById(Conductor conductor);
    
    public void insertConductor(Conductor conductor);
    
    public void updateConductor(Conductor conductor);
    
    public void deleteConductor(Conductor conductor);
}
