/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.transporte.datos;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import com.transporte.domain.Empresa;

/**
 *
 * @author Usuario
 */
@Stateless
public class EmpresaDaoImpl implements EmpresaDao{
    
    @PersistenceContext(unitName="SgaPU")
    EntityManager em;

    @Override
    public List<Empresa> findAllEmpresas() {
        return em.createNamedQuery("Empresa.findAll").getResultList();
    }

    @Override
    public void insertEmpresa(Empresa empresa) {
        em.persist(empresa);
    }

    @Override
    public void updateEmpresa(Empresa empresa) {
        em.merge(empresa);
    }

    @Override
    public void deleteEmpresa(Empresa empresa) {
        Query nativeQuery = em.createNativeQuery("UPDATE Vehiculo set id_empresa = null WHERE id_empresa = ?");
        nativeQuery.setParameter(1, empresa.getIdEmpresa());
        nativeQuery.executeUpdate();
        em.createNamedQuery("Empresa.delete").setParameter("idEmpresa", empresa.getIdEmpresa()).executeUpdate();
    }

    @Override
    public Empresa findEmpresaById(Empresa empresa) {
        return em.find(Empresa.class, empresa.getIdEmpresa());
    }
    
}
